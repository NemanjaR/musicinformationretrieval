import librosa
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pathlib
import os
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
import matplotlib.image as mpimg
import cv2


def calculate_zero_crossing(audio_array):
  return librosa.zero_crossings(audio_array, pad=True)


def calculate_spectral_centroid(audio_array, sample_rate):
  return librosa.feature.spectral_centroid(audio_array, sr=sample_rate)[0]


def calculate_spectral_rolloff(audio_array, sample_rate):
  return librosa.feature.spectral_rolloff(audio_array + 0.01, sr=sample_rate)[0]


def calculate_mfcc(audio_array, sample_rate):
  return librosa.feature.mfcc(audio_array, sr=sample_rate)


def calculate_chroma(audio_array, sample_rate, hop_length=512):
  return librosa.feature.chroma_stft(audio_array, sr=sample_rate, hop_length=hop_length)


def calculate_spectral_bandwidth(audio_array, sr):
  return librosa.feature.spectral_bandwidth(audio_array, sr)


def generate_feature_csv(data_root):
  header = ['chroma_stft', 'spectral_centroid', 'spectral_bandwidth', 'rolloff', 'zero_crossing_rate']
  for i in range(1, 21):
    header.append(f'mfcc{i}')
  header.append('label')
  file = open('gtzan_features.csv', 'w', newline='')
  with file:
    writer = csv.writer(file)
    writer.writerow(header)
  genres = ['blues', 'classical', 'country', 'disco', 'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
  for g in genres:
    for filename in os.listdir(f'{data_root}/GTZAN/genres/{g}'):
      songname = f'{data_root}/GTZAN/genres/{g}/{filename}'
      audio_array, sample_rate = librosa.load(songname, mono=True, duration=30)
      chroma_stft = calculate_chroma(audio_array, sample_rate)
      spec_cent = calculate_spectral_centroid(audio_array, sample_rate)
      spec_bw = calculate_spectral_bandwidth(audio_array, sample_rate)
      rolloff = calculate_spectral_rolloff(audio_array, sample_rate)
      zcr = calculate_zero_crossing(audio_array)
      mfcc = calculate_mfcc(audio_array, sample_rate)
      to_append = [np.mean(chroma_stft), np.mean(spec_cent), np.mean(spec_bw), np.mean(rolloff), np.mean(zcr)]
      for coeff in mfcc:
        to_append.append(np.mean(coeff))
      to_append.append(g)
      file = open('gtzan_features.csv', 'a', newline='')
      with file:
        writer = csv.writer(file)
        writer.writerow(to_append)


def generate_spectrograms():
  cmap = plt.get_cmap('inferno')
  plt.figure(figsize=(1, 1))
  genres = ['blues', 'classical', 'country', 'disco', 'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
  for g in genres:
    print(g)
    pathlib.Path(f'img_data/{g}').mkdir(parents=True, exist_ok=True)
    for filename in os.listdir(f'/home/nemanjarasajski/Documents/GTZAN/genres/{g}'):
      songname = f'/home/nemanjarasajski/Documents/GTZAN/genres/{g}/{filename}'
      audio_array, sample_rate = librosa.load(songname, mono=True, duration=30)
      plt.specgram(audio_array, NFFT=2048, Fs=2, Fc=0, noverlap=128, cmap=cmap, sides='default', mode='default',
                   scale='dB');
      plt.axis('off');
      plt.savefig(f'img_data/{g}/{filename[:-3].replace(".", "")}.png', bbox_inches='tight', pad_inches=0)
      plt.clf()


def gtzan_csv_data(path):
  data = pd.read_csv(path)
  genre_list = data.iloc[:, -1]
  encoder = LabelEncoder()
  y = encoder.fit_transform(genre_list)
  scaler = StandardScaler()
  X = scaler.fit_transform(np.array(data.iloc[:, :-1], dtype=float))
  X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=123, stratify=y)
  return X_train, X_test, y_train, y_test


def gtzan_spectrogram_data(path):
  genres = ['blues', 'classical', 'country', 'disco', 'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
  data = []
  labels = []
  for g in genres:
    for filename in os.listdir(f'{path}/{g}'):
      imgName = f'{path}/{g}/{filename}'
      # img = cv2.imread(imgName,  cv2.IMREAD_GRAYSCALE)
      img = mpimg.imread(imgName)
      img = cv2.cvtColor(img, cv2.COLOR_BGRA2BGR)
      data.append(img)
      labels.append(g)
  encoder = LabelEncoder()
  y = encoder.fit_transform(labels)
  X_train, X_test, y_train, y_test = train_test_split(np.array(data), np.array(y), test_size=0.1, random_state=123, stratify=y)
  return X_train, X_test, y_train, y_test

