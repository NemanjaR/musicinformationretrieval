from __future__ import print_function

from functools import reduce

from keras.models import Sequential
from keras.layers import Dense, Dropout
from sklearn.model_selection import StratifiedKFold
from util.featureExtraction import gtzan_csv_data


def load_data():
  file_path = '../../util/fma_features.csv'
  return gtzan_csv_data(path=file_path)


def create_model(x_train):
  num_classes = 7
  model = Sequential()
  model.add(Dense(512, activation='relu', input_shape=(x_train.shape[1],)))
  model.add(Dropout(0.5))
  model.add(Dense(256, activation='relu'))
  model.add(Dropout(0.2))
  model.add(Dense(128, activation='relu'))
  model.add(Dropout(0.2))
  model.add(Dense(64, activation='relu'))
  model.add(Dropout(0.2))
  model.add(Dense(32, activation='relu'))
  model.add(Dropout(0.1))
  model.add(Dense(num_classes, activation='softmax'))
  model.summary()
  model.compile(loss='sparse_categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])
  return model


def train_and_evaluate_model(model, x_train, y_train, x_train_test, y_train_test, x_test, y_test):
  batch_size = 64
  epochs = 80
  model.fit(x_train, y_train,
            batch_size=batch_size,
            epochs=epochs,
            verbose=1,
            validation_data=(x_train_test, y_train_test))
  score = model.evaluate(x_test, y_test, verbose=0)
  print('Test loss:', score[0])
  print('Test accuracy:', score[1])
  return score


x_train, x_test, y_train, y_test = load_data()

skf = StratifiedKFold(n_splits=10, shuffle=True)
scores = []

for train_index, test_index in skf.split(x_train, y_train):
  model = None  # Clearing the NN.
  model = create_model(x_train[train_index])
  scores.append(train_and_evaluate_model(model, x_train[train_index], y_train[train_index], x_train[test_index], y_train[test_index], x_test, y_test))


print (f'All scores: {scores}')
print (f'Average over 10 folds: {[x / 10 for x in reduce((lambda x, y: [x[0] + y[0], x[1] + y[1]]), scores)]}')
