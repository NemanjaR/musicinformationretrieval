from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, SpatialDropout2D, Activation
from keras.layers import MaxPooling2D, Reshape, Conv2D, BatchNormalization, Lambda
from util.featureExtraction import gtzan_spectrogram_data
import matplotlib.pyplot as plt


def load_data():
  file_path = '../../util/img_data'
  return gtzan_spectrogram_data(path=file_path)


def create_model(x_train):
  model = Sequential()
  model.add(Conv2D(64, (3, 3), padding='same',
                   input_shape=x_train.shape[1:]))
  model.add(BatchNormalization(momentum=0.9))
  model.add(Activation('relu'))
  model.add(MaxPooling2D(3, 3))
  model.add(SpatialDropout2D(0.4))
  model.add(Conv2D(64, (3, 3), padding='same'))
  model.add(BatchNormalization(momentum=0.6))
  model.add(Activation('relu'))
  model.add(MaxPooling2D(3, 3))
  model.add(SpatialDropout2D(0.3))
  model.add(Conv2D(64, (3, 3), padding='same'))
  model.add(BatchNormalization(momentum=0.5))
  model.add(Activation('relu'))
  model.add(MaxPooling2D(3, 3))
  model.add(SpatialDropout2D(0.2))
  model.add(Reshape((2, 128)))
  model.add(LSTM(96, return_sequences=False))
  model.add(Dropout(0.2))

  model.add(Dense(2056))
  model.add(Dropout(0.4))

  model.add(Dense(10))
  model.add(Activation('softmax'))

  model.compile(loss='sparse_categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])

  print(model.summary())
  return model


x_train, x_test, y_train, y_test = load_data()
print(x_train.shape)
model = create_model(x_train)

batch_size = 16
epochs = 70


def show_summary_stats(history):
  plt.plot(history.history['acc'])
  plt.plot(history.history['val_acc'])
  plt.title('model accuracy')
  plt.ylabel('accuracy')
  plt.xlabel('epoch')
  plt.legend(['train', 'test'], loc='upper left')
  plt.show()

  plt.plot(history.history['loss'])
  plt.plot(history.history['val_loss'])
  plt.title('model loss')
  plt.ylabel('loss')
  plt.xlabel('epoch')
  plt.legend(['train', 'test'], loc='upper left')
  plt.show()

history = model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=0)
print(score)
show_summary_stats(history)
